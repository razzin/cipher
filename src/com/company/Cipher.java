package com.company;
import java.lang.*;
public interface Cipher {

    /** deszyfrowanie wiadomosci*/
    String decode(final String message);

    /** szyfrowanie wiadomosci */
    String encode(final String message);
}

class AtBashCipher implements Cipher {

    String allchar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    String cipher=null;
    int len=0, len1=0;

    @Override
    public String decode(String message) {
        return encode(message);
    }

    @Override
    public String encode(String message) {
        len= message.length();
        len1 = allchar.length();
        cipher="";
        for(int i=0; i<len; i++)
        {
            char check= message.charAt(i);
            String b= String.valueOf(check);
            for(int j=0; j<len1; j++)
            {
                String c="";
                if(Character.isUpperCase(check)) {
                    allchar=allchar.toUpperCase();
                }
                else{
                    allchar=allchar.toLowerCase();
                }
                    c = String.valueOf(allchar.charAt(j));
                    if(c.equals(b))
                    {
                        int index=allchar.indexOf(c);
                        int position=(len1-1)-index;
                        cipher+= allchar.charAt(position);
                        break;
                    }
                if( b.equals(" ")){
                    cipher+=" ";
                    break;
                }
            }
        }
        return cipher;
    }
}